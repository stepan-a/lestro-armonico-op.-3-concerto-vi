\version "2.16.2"

#(set-default-paper-size "a4")
#(set-global-staff-size 16)

\paper {
  system-separator-markup = \slashSeparator
}

\header {
  title = "L'Estro armonico"
  subtitle = "Concerto VI (Rv 365)"
  composer = "Antonio Vivaldi"
  opus = "Op.3 No.6"
  tagline = "stepan@ithaca.fr"
}

extendOn = \bassFigureExtendersOn
extendOff = \bassFigureExtendersOff
semprePP = \markup { \dynamic pp \italic sempre }

tempoLargo = {
  \once \override Score.RehearsalMark #'self-alignment-X = #LEFT
  \once \override Score.RehearsalMark #'break-align-symbols = #'(time-signature key-signature)
  \once \override Staff.TimeSignature #'break-align-anchor-alignment = #LEFT
  \mark \markup \bold "Largo"
}

tempoAllegro = {
  \once \override Score.RehearsalMark #'self-alignment-X = #LEFT
  \once \override Score.RehearsalMark #'break-align-symbols = #'(time-signature key-signature)
  \once \override Staff.TimeSignature #'break-align-anchor-alignment = #LEFT
  \mark \markup \bold "Allegro"
}

tempoPresto = {
  \once \override Score.RehearsalMark #'self-alignment-X = #LEFT
  \once \override Score.RehearsalMark #'break-align-symbols = #'(time-signature key-signature)
  \once \override Staff.TimeSignature #'break-align-anchor-alignment = #LEFT
  \mark \markup \bold "Presto"
}

violinI = \relative c'' {

  \tempoAllegro
  \key a \minor
  \time 4/4
  \partial 8
  e8 | a a a a a c16 b a8 c16 b | a8 c16 b a8 g16 f e8 d16 c b8 e | c a a e' a a a a |
  a16 g f e f8 d g g g g | g16 f e d e8 c f f f f | f16 e d c d8 b e e e e | e16 d c b c8 a r16 a c e a8 a |
  a16 a, c e a8 a a16 a, c e bes'8 bes | bes a16 gis a8 b d,2~ | d16 e, gis b d8 d d16 e, gis b d8 d |
  d16 e, gis b d8 e c b16 a f'8 e16 d | e8 a e, gis' a4 r8 e^"Solo" | a a a a a c16 b a8 c16 b |
  a8 c16 b a8 g16 f e8 d 16 c b8 e | c a a e' e g16 f e8 e | a8 g16 f e8 e bes' g16 f e8 e |
  a( cis,) d( f,) g d' a, cis' | d4 r8 d d f16 e d8 d | g f16 e d8 d a' f16 e d8 d | g( b,) c( e,) f c' g, b' |
  c4 r8 e^"Tutti"\p e c16 d e8 e | e c16 d e8 e e f16 g a8 g16 f | e8 d16 c b8 e c a a e'|
  e16^"Solo"\f( a, b a) e'16( a, b a) a'16( a, b a) e'16( a, b a) | a'16( a, b a) e'16( a, b a) g'16( a, b a) e'16( a, b a) |
  g'16( a, b a) e'16( a, b a) f'16( a, b a) d16( a b a) | f'16( a, b a) d16( a b a) f'8 a16 g f8 f |
  d'8. c16 b a g f e8 c16( d e f g a) | d,8 b16( c d e f g) c,8 a16( b c d e f) | b,8 g16( a b c d e) c8 a16( b c d e f) |
  d8 b16( c d e f g) e8 c16( d e f g a) | dis,8 b r e e g16 fis e8 e | a g16 fis e8 e b' g16 fis e8 e |
  c' dis, e g, a e' b, dis' | e4 r8 b^"Tutti" e e e e | e g16 fis e8 g16 fis e8 g16 fis e8 d16 c |
  b8 a16 g fis8 b g e e b' | e e e e e16 d c b c8 a | d d d d d16 c b a b8 g | c c c c c16 b a g a8 fis |
  b b b b b16 a g fis g8 e | a' fis16 g a8 a a fis16 g a8 a | a fis16 g a8 b g4. fis16 e|
  b'8 a16 g fis e dis fis e8 e, r e'^"Solo" | b' a16 g fis e dis fis e8 e, r b''16 c | d8 d d d e, b'16 c d8 d |
  e, b'16 c d8 c16 b c8 a, a a'16 b | c8 c c c d,, a''16 b c8 c | d,, a''16 b c8 b16 a b8 g,, g g''16 a |
  bes8 bes bes bes e, g16 a bes8 bes | cis,16( e) f g a g f e f8 d d, d'16 e | f( d c b) g'( f e d) e8 c c, c'16 d |
  e( c b a) f'( e d c) d8 b b, b'16 c | d( b a gis) e'( d c b) c8 a a, c'16 d | e8 fis16 gis a8 g16 f e8 a,, a c'16 d |
  e8 fis16 gis a8 g16 f e8 a,, a c'16 d | e8 d16 c b a gis b a8 a, a e''^"Tutti" | a a a a a c16 b a8 c16 b |
  a8 c16 b a8 g16 f e8 d16 c b8 gis' | a16(^"Solo" c, b a) e'( c b a) a'( c, b a) e'( c b a) |
  a'( cis, b a) e'( cis b a) a'( cis, b a) a'( g fis e) | fis( fis, e d) a'( fis e d) d'( fis, e d) a'( fis e d) |
  d'( fis, e d) d'( c b a) b( b, a g) d'( b a g) | g'( b, a g) d'( b a g) g'( b, a g) g'( f e d) |
  e( g f e) c'( g f e) c'( a g f) d'( a g fis) | d'( b a g) e'( b a gis) e'( c b a) f'( c b a) |
  f'( d c b) g'( d c b) g'( e d c) a'( e d cis) | a'( fis e d) b'( a gis fis) gis e,^"Tutti" gis b d8 d |
  d16 e, gis b d8 d d16 e, gis b d8 e | c b16 a f'8 e16 d e8 a e, gis' | a4 r8 e^"Solo" a16( g a) e a( g a) e |
  fis( e d) e fis g a fis g( fis g) d g( f g) d | e( d c) d e f g e f( e f) c f( e f) c |
  d( c b) c d e f d e( d e) b e( d e) b | c^"Tutti" a c e a8 a a16 a, c e a8 a | a16 a, c e bes'8 bes bes a16 gis a8 b |
  d,2~ d16 e, gis b d8 d | d16 e, gis b d8 d d16 e, gis b d8 e | c b16 a f'8 e16 d e8 a e, gis' | a1\fermata \bar "||"
  \break
  
  \tempoLargo
  \key d \minor
  \time 4/4
  d,16^"Solo e Cantabile"( e f e) d( f e d) e8 a, r4 | e'16( d e f) e( f g a) f e d8 r16 f g a | 
  bes( a g f) bes( a g f) e( d e f) e( f g e) | a( g f e) a( g f e) d( cis d e) d( e f d) |
  g( f e d) g( f e d ) cis b a8 r16 e' f g | f8 e16 d cis8. d16 d4 r16 d e f |
  e16.( f32) e16.( f32) e16.( f32) e16.( f32) e8 d16 c b8.\trill a16 |
  a4 r16 a b cis d32( cis? d e) d32( cis d e) d32( cis d e) c( b? c d) |
  b4\trill r16 b c d e32( d e f) e32( d e f) e32( d e f) d( cis d e) |
  cis4\trill r16 cis d e f32( e f g) f32( e f g) f32( e f g) e( d e f) |
  d4\trill r16 d e f g32( a bes a g f e d) g( a bes a g f e d) |
  \times 2/3 {cis16( b a)} \times 2/3 {f'( e d)} cis?8.\trill d16 d4 r16 d f d |
  es16.( f32) es16.( f32) es16.( f32) es?16.( fis32) g4. a16 g | f8 e16 d cis8.\trill d16 d2\fermata \bar "||"
  \break
  
  \tempoPresto
  \key a \minor
  \time 2/4
  r8 e a b | c r b r | a4 r8 a | g f e d | c b a b | c r d r | e4 r| r8 e\p a b | c r b r | a4 r8 a  | g f e d | c b a b | c r d r | e4 r8 e\f| c' c c c | 
  c16 d, d c' c d, d c' | b8 b b b | b16 c, c b' b c, c b' | a8 a a a | gis fis16 gis a8 b16 a | gis8 fis16 gis a8 b16 a | gis8 fis16 e f8 e16 d | e4. f8 | e4. f8 | 
  e d16 e f8 f | e8 d16 e f8 f | e fis16 gis a8 a16 b | c8 c16 d e4 | r8 a, e gis | a e^"Solo" a16 c b d | c8 r b r | a4 r8 a | g16 a f g e f d e | c d b c a8 a' | 
  g16 a f g e f d e | c d b c a8 e' | e16 g f g e g f e | a( g f e) a( g f e) | f d cis d f d cis d | g( f e d) g( f e d) |
  e c b c e c b c | f( e d c) f( e d c) | d b a b d b a b | e( d c b) e( d c b) | c a c e a b a b |c, a c e a b a b | dis, b dis fis a b a b | dis, b dis fis a b a b |
  g8 fis16 e dis8.\trill e16 | e8 b^"Tutti" e fis | g r fis r | e b\p e fis | g r fis r | e4 r8 b^"Solo"\f | g'16 a g a g a g a | g8 fis16( g) a( g) fis( e) |
  fis g fis g fis g fis g | fis8 e16( fis) g( fis) e( dis) | e fis e fis e fis e fis | e8 dis16( e) fis( e) dis( cis) | dis8 cis16 dis b8 b'^"Tutti" | gis fis16 gis a8 b16 a |
  gis8 fis16 gis a8 b16 a | gis8 fis16 e f8 e16 d | e4. f8 | e4. f8 | e d16 e f8 f | e d16 e f8 f | e fis16 gis a8 a16 b | c8 c16 d e8 e,^"Solo" | a a16 b c8 c16 d |
  e8 e, e e | a a16 b c8 c16 d | e8 e, e e | a16 a, cis e a a, cis e | a a, cis e a a, cis e | g a, cis e g a, cis e | g a, cis e g a, cis e | fis d, fis a d d, fis a |
  d d, fis a d d, fis a | c d, fis a c d, fis a | c d, fis a c d, fis a | b g a b c d e f | g c, d e f g a b | c g e g c g e g | c g e g c g e g | c g d g c g d g |
  c g d g c g d g | b g d g b g d g | b g d g b g d b' | c8 g,^"Tutti" c d | e r d r | c g\p c d | e r d r | c4 r8 e\f | a a a a | a g16 fis b8 a | g fis16 e b8 dis |
  e b e fis | g r fis r | e b\p e fis | g r fis r | e4 r8 e\f | c' c c c | c16 d, d c' c d, d c' | b8 b b b | b16 c, c b' b c, c b' | a8 a a a |
  gis16^"Solo" e gis e b' e, gis e | b' e, gis e b' e, gis e | a e e e a e e e | b' e, e e b' e, e e | c' e, e e c' e, e e | d' e, e e d' e, e e | e'8 e,^"Tutti" a b |
  c r b r | a4 r8 a | g f e d | c b a b | c r d r | e4 r8 e^"Solo" | c'16 d c d c d c d | c8 b16( c) d( c) b( a) | b c b c b c b c| b8 a16( b) c( b) a( gis) |
  a b a b a b a b | gis8^"Tutti" fis16 gis a8 b16 a | gis8 fis16 gis a8 b16 a | gis8 fis16 e f8 e16 d | e a^"Solo" gis a e8 f | e16 a gis a e8 f |
  e^"Tutti" d16 e f8 f | e d16 e f8 f | e fis16^"Solo" gis a8 a16 b | c8 c16 d e8 e,^"Tutti" | f e a c, | b a' b, gis' | a e^"Solo" a a | gis16 fis e fis g8 a |
  fis16 e d e f8 g | e16 d c d e8 f | d16 c b c d8 e | c16 a^"Tutti" c e a8 c, | b a' b, gis' | a2\fermata | \bar "|."
    
}

violinII = \relative c'' {
  
  \tempoAllegro
  \key a \minor
  \time 4/4
  \partial 8
  e8 | a a a a a c16 b a8 c16 b | a8 c16 b a8 g16 f e8 d16 c b8 e | c a a e' a a a a |
  a16 g f e f8 d g g g g | g16 f e d e8 c f f f f | f16 e d c d8 b e e e e | e16 d c b c8 a r16 a c e a8 a |
  a16 a, c e a8 a a16 a, c e bes'8 bes | bes a16 gis a8 b d,2~ | d16 e, gis b d8 d d16 e, gis b d8 d |
  d16 e, gis b d8 e c b16 a f'8 e16 d | e8 a e, gis' a4 r |
  a,, r a r | a r a r |a r cis r | cis r cis r | cis r r2 | r b4 r | b r b r | b r r2 |
  r4 r8 e'\p e c16 d e8 e | e c16 d e8 e e f16 g a8 g16 f | e8 d16 c b8 e c a a4 |
  a, r a r | a r a r | a r d r | d r d r | b r c r | b r a r | g r a r | b r c r | b r g' r | fis r g r | a r r2 |
  r4 r8 b e e e e | e g16 fis e8 g16 fis e8 g16 fis e8 d16 c |
  b8 a16 g fis8 b g e e b' | e e e e e16 d c b c8 a | d d d d d16 c b a b8 g | c c c c c16 b a g a8 fis |
  b b b b b16 a g fis g8 e | a' fis16 g a8 a a fis16 g a8 a | a fis16 g a8 b g4. fis16 e|
  b'8 a16 g fis e dis fis e8 e, r4 | R1 | b'4 r gis r | gis? r a r | a, r fis' r | fis? r g r | g r g r | e r d r |
  d r c r | c r b r | b r a r | a r a r | a r a r | r2 r4 r8 e'' | a a a a a c16 b a8 c16 b |
  a8 c16 b a8 g16 f e8 d16 c b8 e | c a r4 a, r | a r a r | d r d r | d r g, r | g r g r | c r f r | g r a r | b r c r |
  d r r16 e, gis b d8 d | d16 e, gis b d8 d d16 e, gis b d8 e | c b16 a f'8 e16 d e8 a e, gis' | a4 r cis,, r | d r b r |
  c r a r | b r gis r | c'16 a c e a8 a a16 a, c e a8 a | a16 a, c e bes'8 bes bes a16 gis a8 b |
  d,2~ d16 e, gis b d8 d | d16 e, gis b d8 d d16 e, gis b d8 e | c b16 a f'8 e16 d e8 a e, gis' | a1\fermata \bar "||"
  
  
  \tempoLargo
  \key d \minor
  \time 4/4
  a1~-\semprePP | a | bes | a | g | f4 r8 e f2 | e4. e8 e d16 c b8.\trill c16 | c4 cis d2~ | d e~ |
  e f~ | f g~ | g8 f e8.\trill f16 f4 r | r2 r4 r8 a16 g | f8 e16 d cis8.\trill d16 d2\fermata \bar "||"
    
  
  \tempoPresto
  \key a \minor
  \time 2/4
  r8 e a b | c r b r | a4 r8 a | g f e d | c b a b | c r d r | e4 r| r8 e\p a b | c r b r | a4 r8 a | g f e d | c b a b | c r d r | e4 r8 e\f|
  c' c c c | c16 d, d c' c d, d c' | b8 b b b | b16 c, c b' b c, c b' | a8 a a a | gis fis16 gis a8 b16 a |  gis8 fis16 gis a8 b16 a | gis8 fis16 e f8 e16 d |
  e4. f8 | e4. f8 | e d16 e f8 f | e8 d16 e f8 f | e fis16 gis a8 a16 b | c8 c16 d e4 | r8 a, e gis | a4 r | R2*19 |
  r8 b, e fis | g r fis r | e b\p e fis | g r fis r | e4 r | R2*6 | r4 r8 b' | gis fis16 gis a8 b16 a | 
  gis8 fis16 gis a8 b16 a | gis8 fis16 e f8 e16 d | e4. f8 | e4. f8 | e d16 e f8 f | e d16 e f8 f | e fis16 gis a8 a16 b | c8 c16 d e4 | 
  R2*20 | r8 g,, c d | e r d r | c g\p c d | e r d r | c4 r8 e\f | a a a a | a g16 fis b8 a | g fis16 e b8 dis |
  e b e fis | g r fis r | e b\p e fis | g r fis r | e4 r8 e\f | c' c c c | c16 d, d c' c d, d c' | b8 b b b | b16 c, c b' b c, c b' | a8 a a a |
  gis4 r | R2*5 | r8 e a b | c r b r | a4 r8 a | g f e d | c b a b | c r d r | e4 r | R2*5 |
  gis8 fis16 gis a8 b16 a | gis8 fis16 gis a8 b16 a | gis8 fis16 e f8 e16 d | e4 r |
  R2 | e8 d16 e f8 f | e d16 e f8 f | e4 r | r r8 e | f e a c, | b a' b, gis' | a4 r | R2*4 |r16 a, c e a8 c, | b a' b, gis' | a2\fermata | \bar "|."
  
  
}

violinIPart = \new Staff \with {
  instrumentName = "Violino I"
  shortInstrumentName = "Vl.I"
  midiInstrument = "violin"
} \violinI

violinIIPart = \new Staff \with {
  instrumentName = "Violino II"
  shortInstrumentName = "Vl.II"
  midiInstrument = "violin"
} \violinII

\score {
  <<
    \violinIPart
    \violinIIPart
  >>
  
  \layout {
    \context {
      \RemoveEmptyStaffContext 
      \override VerticalAxisGroup #'remove-first = ##t
    }
  }
  \midi {
    \context {
      \Score
      tempoWholesPerMinute = #(ly:make-moment 100 4)
    }
  }
}